function fib(N) {
	let a = 0;
	let b = 1;
	for (let i = 0; i < N; i++) {
		let _ = a;
		a = b;
		b = _ + b;
	}
	return a;
}